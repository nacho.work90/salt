install_apache:
  pkg.installed:
    - pkgs:
      - apache2

index_html:
  file.managed:
    - name: /var/www/html/index.html
    - user: apache
    - group: www
    - mode: 644
    - source: salt://apache/templates/index.html

apache_service:
  service.running:
    - enabled: True
